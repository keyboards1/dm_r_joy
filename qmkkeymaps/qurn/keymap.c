/* A standard layout for the Dactyl Manuform 5x6 Keyboard */ 

#include QMK_KEYBOARD_H
//#include keymap_german.h

#define _COLMAK 0
#define _LOWER 1
#define _RAISE 2

#define RAISE MO(_RAISE)
#define LOWER MO(_LOWER)

//Hit ALT one for mouse, twice for arrowkeys, tripple for scrollwheel
uint8_t stickMode = 0; // 0 Mousecursor; 1 arrowkeys; 2 scrollwheel

////Tap Dance section

typedef struct {
 bool is_press_action;
  int state;
} tap;

enum {
  SINGLE_TAP = 1,
  SINGLE_HOLD = 2,
  DOUBLE_TAP = 3,
  DOUBLE_HOLD = 4,
  DOUBLE_SINGLE_TAP = 5, //send two single taps
  TRIPLE_TAP = 6,
  TRIPLE_HOLD = 7
};

//Tap dance enums
enum {
  X_ALT = 0
  //, XM_ALT = 0
};

int cur_dance (qk_tap_dance_state_t *state);

//for the x tap dance. Put it here so it can be used in any keymap
void x_finished (qk_tap_dance_state_t *state, void *user_data);
void x_reset (qk_tap_dance_state_t *state, void *user_data);



enum custom_keycodes {
  ESC_JS,
  M_HOLD
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	switch (keycode) {
		case ESC_JS:
		   if (record->event.pressed) {
		      tap_code(KC_ESC);
			  stickMode = 0;
		   } else {
		   }
		case M_HOLD:
		   if (record->event.pressed) {
		      register_code(KC_MS_BTN1);
		   } else {
		      unregister_code(KC_MS_BTN1);
		   }
	   break;
	}    	return true;
}


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_COLMAK] = LAYOUT_5x6(
     KC_EQL , KC_1  , KC_2  , KC_3  , KC_4  , KC_5  ,                         KC_6  , KC_7  , KC_8  , KC_9  , KC_0    ,KC_QUOT,
	 KC_TAB , KC_Q  , KC_W  , KC_F  , KC_P  , KC_G  ,                         KC_J  , KC_L  , KC_U  , KC_Y  , KC_SCLN ,KC_MINS,
     KC_LSFT, KC_A  , KC_R  , KC_S  , KC_T  , KC_D  ,                         KC_H  , KC_N  , KC_E  , KC_I  , KC_O    ,KC_RSFT,
     KC_LCTL, KC_Z  , KC_X  , KC_C  , KC_V  , KC_B  ,                         KC_K  , KC_M  ,KC_COMM, KC_DOT, KC_SLSH ,KC_BSLASH,

                      //KC_LBRC,KC_RBRC,                                                          M_HOLD, KC_MS_BTN2,
                       KC_MS_BTN1, KC_MS_BTN2,                                                     KC_LBRC,KC_RBRC,
                                      RAISE,   KC_SPC,                   KC_MS_BTN3, KC_BSPC,
                                      LOWER,   KC_ENT,                   _______,    KC_LGUI,
                                      KC_TAB, KC_ESC,                    TD(X_ALT),  KC_DEL
  ),

  [_LOWER] = LAYOUT_5x6(

     KC_TILD,KC_EXLM, KC_AT ,KC_HASH,KC_DLR ,KC_PERC,                        KC_CIRC,KC_AMPR,KC_ASTR,KC_LPRN,KC_RPRN,KC_DEL,
     _______,_______,_______,_______,_______,KC_LBRC,                        KC_RBRC, KC_P7 , KC_P8 , KC_P9 ,_______,KC_PLUS,
     _______,KC_HOME,KC_PGUP,KC_PGDN,KC_END ,KC_LPRN,                        KC_RPRN, KC_P4 , KC_P5 , KC_P6 ,KC_MINS,KC_PIPE,
     _______,_______,_______,_______,_______,_______,                        _______, KC_P1 , KC_P2 , KC_P3 ,KC_EQL ,KC_UNDS,
                                             _______,KC_PSCR,            _______, KC_P0,
                                             _______,_______,            _______,_______,
                                             _______,_______,            _______,_______,
                                             _______,KC_GRV,             _______,_______

  ),

  [_RAISE] = LAYOUT_5x6(
     KC_F12 , KC_F1 , KC_F2 , KC_F3 , KC_F4 , KC_F5 ,                        KC_F6  , KC_F7 , KC_F8 , KC_F9 ,KC_F10 ,KC_F11 ,
     _______,_______,_______,_______,_______,KC_LBRC,                        KC_DOWN,KC_RGHT,KC_NLCK,KC_INS ,KC_SLCK,KC_MUTE,
     _______,KC_LEFT,KC_UP  ,KC_DOWN,KC_RGHT,KC_LPRN,                        KC_LEFT,KC_MPRV,KC_MPLY,KC_MNXT,_______,KC_VOLU,
     _______,_______,_______,_______,_______,_______,                        KC_UP,_______,_______,_______,_______,KC_VOLD,
                                             _______,_______,            KC_EQL ,_______,
                                             _______,_______,            _______,_______,
                                             _______,_______,            _______,_______,
                                             _______,_______,            _______,_______
  ),
};

#include "analog.c"
#include "pointing_device.h"
#include "pincontrol.h"

// Joystick
// Set Pins
uint8_t xPin  = 2;   // VRx / /B4
uint8_t yPin  = 3;   // VRy // B5

// Set Parameters
uint16_t minAxisValue = 0;
uint16_t maxAxisValue = 1023;

float wheelSpeed = 0.010;
float cursorSpeed = 0.0025;

uint8_t speedRegulator = 10;  // Lower Values Create Faster Movement

int8_t xPolarity = 1;
int8_t yPolarity = -1;

uint8_t cursorTimeout = 10;

int16_t xOrigin = 503;
int16_t yOrigin = 510;

uint16_t lastCursor = 0;

int16_t axisCoordinate(uint8_t pin, uint16_t origin) {
    int8_t  direction;
    int16_t distanceFromOrigin;
    int16_t range;

    int16_t position = analogRead(pin);

    if (origin == position) {
        return 0;
    } else if (origin > position) {
        distanceFromOrigin = origin - position;
        range              = origin - minAxisValue;
        direction          = -1;
    } else {
        distanceFromOrigin = position - origin;
        range              = maxAxisValue - origin;
        direction          = 1;
    }

    float   percent    = (float)distanceFromOrigin / range;
    int16_t coordinate = (int16_t)(percent * 100);
    if (coordinate < 0) {
        return 0;
    } else if (coordinate > 100) {
        return 100 * direction;
    } else {
        return coordinate * direction;
    }
}

int8_t axisToMouseComponent(uint8_t pin, int16_t origin, float maxSpeed, int8_t polarity) {
    int coordinate = axisCoordinate(pin, origin);
    if (coordinate == 0) {
        return 0;
    } else {
        return coordinate * maxSpeed * polarity * abs(coordinate);
    }
}

int16_t arrowSpeed = 5000;

uint16_t curTime = 0;
uint16_t lastTime = 0;
uint16_t nextTime = 0;

bool js_x_was_tapped = false;
bool js_y_was_tapped = false;
int16_t js_tap_threshold_upper = 200;
int16_t js_tap_threshold_lower = 3;


bool tap_js(uint8_t pin, uint16_t origin, uint16_t keycode0, uint16_t keycode1, bool js_was_tapped) {
    int8_t  direction;
    int16_t distanceFromOrigin;

    int16_t position = analogRead(pin);

    if (origin == position) {
        return false;
    } else if (origin > position) {
        distanceFromOrigin = origin - position;
        direction          = -1;
    } else {
        distanceFromOrigin = position - origin;
        direction          = 1;
    }

	nextTime = arrowSpeed / distanceFromOrigin;
	curTime = timer_read();

	//char snum[5];
	//itoa(distanceFromOrigin, snum, 10);
	//SEND_STRING(" ");
	//send_string(snum);
	if (distanceFromOrigin < js_tap_threshold_lower) {
		js_was_tapped = false;
	}
    if (distanceFromOrigin > js_tap_threshold_upper) {
		//case js is held a wider angle
		if (direction > 0) {
			if (curTime - lastTime > nextTime) {
				tap_code(keycode0);
				lastTime = timer_read();
			}
    	} else {
			if (curTime - lastTime > nextTime) {
				tap_code(keycode1);
				lastTime = timer_read();
			}
    	}
	} else if (js_was_tapped == false && distanceFromOrigin > js_tap_threshold_lower) {
		//case js is just tapped a little angle
		if (direction > 0) {
			tap_code(keycode0);
    	} else {
			tap_code(keycode1);
    	}
		js_was_tapped = true;
	}
	return js_was_tapped;
}

void pointing_device_task(void) {
    report_mouse_t report = pointing_device_get_report();
    // todo read as one vector
    if (timer_elapsed(lastCursor) > cursorTimeout) {
       lastCursor = timer_read();
		if (stickMode == 0) {
			report.x   = axisToMouseComponent(xPin, xOrigin, cursorSpeed, xPolarity);
        	report.y   = axisToMouseComponent(yPin, yOrigin, cursorSpeed, yPolarity);
		}
		if (stickMode == 1) {
			//DEBUG
			//int num = analogRead(yPin);
			//int num = axisToMouseComponent(xPin, xOrigin, cursorSpeed, xPolarity);
			//int num = (xOrigin - analogRead(xPin))/20;
			//char snum[5];
			//itoa(num, snum, 10);
			//SEND_STRING(" ");
			//send_string(snum);

			js_x_was_tapped = tap_js(xPin, xOrigin, KC_RIGHT, KC_LEFT, js_x_was_tapped);
			js_y_was_tapped = tap_js(yPin, yOrigin, KC_UP, KC_DOWN, js_y_was_tapped);
		}
		if (stickMode == 2) {
			report.h   = axisToMouseComponent(xPin, xOrigin, wheelSpeed, xPolarity);
        	report.v   = axisToMouseComponent(yPin, yOrigin, wheelSpeed, -yPolarity);
		}
    }
    pointing_device_set_report(report);
    pointing_device_send();
}

void matrix_init_keymap(void) {
    // Account for drift
    xOrigin = analogRead(xPin);
    yOrigin = analogRead(yPin);
}



// Tap dance again
//
//
int cur_dance (qk_tap_dance_state_t *state) {
  if (state->count == 1) {
    if (state->interrupted || !state->pressed)  return SINGLE_TAP;
    //key has not been interrupted, but they key is still held. Means you want to send a 'HOLD'.
    else return SINGLE_HOLD;
  }
  else if (state->count == 2) {
    /*
     * DOUBLE_SINGLE_TAP is to distinguish between typing "pepper", and actually wanting a double tap
     * action when hitting 'pp'. Suggested use case for this return value is when you want to send two
     * keystrokes of the key, and not the 'double tap' action/macro.
    */
    if (state->interrupted) return DOUBLE_SINGLE_TAP;
    else if (state->pressed) return DOUBLE_HOLD;
    else return DOUBLE_TAP;
  }
  //Assumes no one is trying to type the same letter three times (at least not quickly).
  //If your tap dance key is 'KC_W', and you want to type "www." quickly - then you will need to add
  //an exception here to return a 'TRIPLE_SINGLE_TAP', and define that enum just like 'DOUBLE_SINGLE_TAP'
  if (state->count == 3) {
    if (state->interrupted || !state->pressed)  return TRIPLE_TAP;
    else return TRIPLE_HOLD;
  }
  else return 8; //magic number. At some point this method will expand to work for more presses
}

static tap xtap_state = {
  .is_press_action = true,
  .state = 0
};


//buttons tried
//0x00 - 0xed

void x_finished (qk_tap_dance_state_t *state, void *user_data) {
  xtap_state.state = cur_dance(state);
  switch (xtap_state.state) {
    case SINGLE_TAP: {
		if (stickMode < 2) {
			stickMode = 0;
		}
		else {
			stickMode = 0; 
		}
		break;
	}
    case SINGLE_HOLD: register_code(KC_LALT); break;
    case DOUBLE_TAP: stickMode = 1; break;
    case DOUBLE_HOLD: register_code(KC_RALT); break;
    //Last case is for fast typing. Assuming your key is `f`:
    //For example, when typing the word `buffer`, and you want to make sure that you send `ff` and not `Esc`.
    //In order to type `ff` when typing fast, the next character will have to be hit within the `TAPPING_TERM`, which by default is 200ms.
  }
}

void x_reset (qk_tap_dance_state_t *state, void *user_data) {
  switch (xtap_state.state) {
    case SINGLE_HOLD: unregister_code(KC_LALT); break;
    case DOUBLE_HOLD: unregister_code(KC_RALT);
  }
  xtap_state.state = 0;
}

//static tap xmtap_state = {
//  .is_press_action = true,
//  .state = 0
//};
//
//void xm_finished (qk_tap_dance_state_t *state, void *user_data) {
//  xmtap_state.state = cur_dance(state);
//  switch (xmtap_state.state) {
//    case SINGLE_TAP: {
//		if (stickMode < 2) {
//			stickMode = stickMode + 1;
//		}
//		else {
//			stickMode = 0; 
//		}
//		break;
//	}
//    case SINGLE_HOLD: register_code(KC_LGUI); break;
//  }
//}
//
//void xm_reset (qk_tap_dance_state_t *state, void *user_data) {
//  switch (xmtap_state.state) {
//    case SINGLE_HOLD: unregister_code(KC_LGUI); break;
//  }
//  xmtap_state.state = 0;
//}

qk_tap_dance_action_t tap_dance_actions[] = {
  [X_ALT]     = ACTION_TAP_DANCE_FN_ADVANCED(NULL,x_finished, x_reset),
  //[XM_ALT]     = ACTION_TAP_DANCE_FN_ADVANCED(NULL,xm_finished, xm_reset)
};



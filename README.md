This is a dactyl manuform with a joystick in the right thumbcluster.
If you want the final stl its the boolean/right.stl

The joystick is a ALPSALPINE RKJV1224005.

The Joystick has 3 Modes:
-Mouse movement
-Scrolling
-Tap arrow keys in a frequency corresponding to the tilt angle

The state of the code is works-for-me

Project wise i started with the dactyl manuform code:
-removed the wire holder
-moved the buttons on the thumbcluster for space
-removed one button and reconnected everything
-removed the screws

FreeCAD:
-place hotswaps on all buttons
-make joystick place
-make joystick hole (negative)
-add skrews
-export positives to p2.stl and negative to m1.stl

OpenSCAD Boolean operations:
-add all from FreeCAD model (p2.stl)
-remove hole (m1.stl)
-export to stl

-> CUDA -> printed with Ender 3
